# BlackBlueRedGreen Syntax Highlighting Theme #

![Logo](https://gitlab.com/julrich/blackblueredgreen/-/raw/main/images/logo.png)

Light syntax highlighting theme for different IDEs and websites (user styles).

![Screenshot](https://gitlab.com/julrich/blackblueredgreen/-/raw/main/images/Screenshot_VSC_C++.png)

See https://gitlab.com/julrich/blackblueredgreen for more details.


<details>
	<summary>Manual Installation from Repository</summary>
	<ol>
		<li>Open the <code>extensions</code> directory of Visual Studio Code. It is located at <code>&lt;user home&gt;/.vscode/extensions</code>.</li>
		<li>Create a directory called <code>blackblueredgreen-theme-&lt;version&gt;</code> where you replace <code>&lt;version&gt;</code> with the current version of the theme (see <code>package.json</code>).</li>
		<li>Copy the content of this directory (<code>VisualStudioCode</code>) into the directory created in the previous step.</li>
		<li>(Re)start Visual Studio Code</li>
		<li>From the main menu select "File" -> "Preferences" -> "Color Theme"</li>
		<li>Select "BlackBlueRedGreen"</li>
	</ol>
</detail>
