# UserCSS Website Stylesheet #

## Installation ##
1. Install a userstyle manager browser extension (for example: [Stylus](https://add0n.com/stylus.html))
2. Install the `BlackBlueRedGree.user.css` according to the instructions of the userstyle manager
   - For example for Stylus:
     1. Open [BlackBlueRedGreen.user.css](https://gitlab.com/julrich/blackblueredgreen/raw/main/UserCSS/BlackBlueRedGreen.user.css)
	 2. Click "Install style"

## Supported Websites ##
- [GitHub](https://github.com)
- [GitLab](https://gitlab.com)
- Self hosted GitLab
  - Works automatically if the URL matches the regular expression `.*\bgitlab\b.*`. Else you have to
  adapt the `@-moz-document` line in the `BlackBlueRedGree.user.css`.