# Eclipse Syntax Coloring #

## Installation ##
1. Start Eclipse
2. From the main menu select "File" -> "Import..."
3. In the Import dialog, open the "General" category, select "Preferences" and click "Next >"
4. Click "Browse..." and select the `.epf` file for a language/plugin or the `all.epf` to import the color scheme in all supported languages/plugins.
5. Click "Finish"

### TM4E (TextMate support in the Eclipse IDE) ###
When using the [TM4E (TextMate support in the Eclipse IDE)](https://projects.eclipse.org/projects/technology.tm4e) plugin, you also need to follow these steps:
1. Open the "Preferences" dialog
2. Open the "TextMate" -> "Theme" page
3. Click "Add..." and select the `BlackBlueRedGreen.tm4e.css` file
4. Open the "TextMate" -> "Grammar" page
5. For each language (Scope) which should use the BlackBlueRedGreen theme:
	1. Select the scope in the "Create, edit or remove TextMate grammars" list
	2. Open the "Theme" tab
	3. Ensure "Use '...' theme." is selected in "Theme associations"
	4. Click "Edit..."
	5. Select "BlackBlueRedGreen.tm4e" in the "Use the Theme" combobox
	6. Click "Finish"
