# BlackBlueRedGreen Syntax Highlighting Theme #

![Logo](images/logo.svg)

Light syntax highlighting theme for different IDEs and websites (user styles).

![Screenshot](images/Screenshot_Eclipse_C++.png)

## Installation ##

See the `README.md` files in the corresponding directories (Eclipse, QtCreator, UserCSS, VisualStudioCode).

## Description ##
The theme uses a limited set of colors and uses font styles (mainly bold) for additional highlighting.

The main styles are (as the name of the theme suggests):

<table>
	<tr>
		<th width="150">Style</th>
		<th>Usage</th>
		<th>Examples</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>black<br/><code>#000000</code></td>
		<td>"Normal" text, local variables, operators</td>
		<td><tt>myVar</tt></td>
		<td></td>
	</tr>
	<tr>
		<td><b>black bold<br/><code>#000000</code></b></td>
		<td>Functions, methods, classes, types</td>
		<td><tt><b>MyClass</b> myObj;<br/>
		myObj.<b>doSomething</b>();</tt></td>
		<td>Not all IDEs highlight classes and types in bold in all places</td>
	</tr>
	<tr>
		<td>dark blue<br/><code>#0000c8</code></td>
		<td>Member variables</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/memberVar.svg" height="15"/></td>
		<td></td>
	</tr>
	<tr>
		<td><b>blue bold<br/><code>#0000ff</code></b></td>
		<td>Keywords, built-in types</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/keywords.svg"/></td>
		<td></td>
	</tr>
	<tr>
		<td>red<br/><code>#ff0000</code></td>
		<td>Comments</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/comments.svg"/></td>
		<td></td>
	</tr>
	<tr>
		<td>green<br/><code>#008000</code></td>
		<td>Strings, characters, regular expressions</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/strings.svg"/></td>
		<td></td>
	</tr>
</table>

Additional styles are:

<table>
	<tr>
		<th width="150">Style</th>
		<th>Usage</th>
		<th>Examples</th>
		<th>Notes</th>
	</tr>
	<tr>
		<td>magenta<br/><code>#ff00ff</code></td>
		<td>Numbers</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/number.svg"/></td>
		<td></td>
	</tr>
	<tr>
		<td>purple<br/><code>#800080</code></td>
		<td>Markup attributes</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/attribute.svg"/></td>
		<td></td>
	</tr>
	<tr>
		<td>blue<br/><code>#0000ff</code></td>
		<td>Variable substitution (string interpolation)</td>
		<td><imd src="https://julrich.gitlab.io/blackblueredgreen/images/variableSubstitution.svg"/></td>
		<td>Not supported by all IDEs.</td>
	</tr>
	<tr>
		<td>gray blue<br/><code>#3F5FBF</code></td>
		<td>Documentation comments</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/documentation.svg"/></td>
		<td>Not supported by all IDEs</td>
	</tr>
	<tr>
		<td><em>black italic<br/><code>#000000</code></em></td>
		<td>Global variables</td>
		<td><tt><em>std::cout</em></tt></td>
		<td>Not supported by all IDEs</td>
	</tr>
		<tr>
		<td><em><b>black italic bold<br/><code>#000000</code></b></em></td>
		<td>Static methods</td>
		<td><tt><em><b>staticMethod()</b></em></tt></td>
		<td>Not supported by all IDEs</td>
	</tr>
	<tr>
		<td><em>dark blue italic<br/><code>#0000c8</code></em></td>
		<td>Static member variables, enumerators</td>
		<td><img src="https://julrich.gitlab.io/blackblueredgreen/images/static.svg"/></td>
		<td>Not supported by all IDEs</td>
	</tr>
</table>