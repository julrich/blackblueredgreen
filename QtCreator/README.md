# QtCreator Color Scheme #

## Installation ##
1. Copy the file `BlackBlueRedGreen.xml` into the `styles` directory of the QtCreator settings directory. The location of the directory depends on the operating system:
   - Windows: `%USERPROFILE%\AppData\Roaming\QtProject\qtcreator\styles`
2. Start QtCreator
3. Open the Options dialog (e.g. from the main menu "Tools" -> "Options...")
4. In the menu on the left, select "Text Editor"
5. Select "BlackBlueRedGreen" in the drop down under "Color Scheme for Theme "...""
6. Click "OK"